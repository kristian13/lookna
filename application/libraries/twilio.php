<?php if (!defined('BASEPATH')) exit('No direct script access allowed');




class Twilio {


    function __construct() {     
        require_once "third-party/twilio/Twilio.php";

        $this->ci =& get_instance();
        $this->ci->load->config('twilio');

        $this->sid   = 'AC27e9a992f2a39c7b1c198d9f3a35f376';
        $this->token = '68d2619603491e7fb75d3167f2a0c879';
        $this->from  = '+13202868831';
    }

    function send_verification($recipient, $verification_code) {

        $sid   = $this->sid;
        $token = $this->token; 
        $from  = $this->from; 

        $code = $verification_code;

        $client = new Services_Twilio($sid, $token);

        try {
            /*$message = $client->account->messages->sendMessage(
            $from, // From a valid Twilio number
            $recipient, // Text this number
                "Your Verification code is ".$code
            ); */

            $message = $client->account->sms_messages->create($from, $recipient, "Your Lookna verification code is ".$code, array());

            $message->verification_status = 1;
            $message->verification_code = $code;

            //debug($message); exit();
        } catch (Exception $e) {
            $message['verification_status'] = 0;
            $message['verification_error'] = $e->getMessage();
            $message = (object)$message;

            //debug($message); exit();
        }

        /* TEST */


        return $message;

        //ANOTHER WAY TO SEND SMS https://www.twilio.com/help/faq/twilio-basics/how-does-twilios-free-trial-work
        // Get the PHP helper library from twilio.com/docs/php/install
        //require_once('/path/to/twilio-php/Services/Twilio.php'); // Loads the library
         
        // Your Account Sid and Auth Token from twilio.com/user/account
        //$sid = "ACf32d0dc9481258bf5717452b5ca8638c"; 
        //$token = "{{ auth_token }}"; 
        /* 
        $client = new Services_Twilio($sid, $token);
         
        $sms = $client->account->sms_messages->create("+15005550006", "+639067442380", "All in the game, yo", array());
        echo $sms->sid;
        */

    }

    function validate_phone($phone, $countrycode) {
        $sid = $this->sid;
        $token = $this->token; 

        $client = new Lookups_Services_Twilio($sid, $token);
         
        $number = $client->phone_numbers->get($phone, array("CountryCode" => $countrycode, "Type" => "carrier"));

        return $number->phone_number;
    }

    function phonecall_verification($recipient, $verification_code) {

        $sid = $this->sid;
        $token = $this->token; 
        $from = $this->from; 

        $code = $verification_code;

        $client = new Services_Twilio($sid, $token);

        try {
        //$phonecall = $client->account->calls->create($from, $recipient, base_url()."twiml.php?verification_code=$code", array());
        $phonecall = $client->account->calls->create($from, $recipient, "https://forall3.com/lninc1.0/twiml.php?verification_code=$code", array());

        $phonecall->verification_status = 1;
        $phonecall->verification_code = $code;

        } catch (Exception $e) {
            $phonecall['verification_status'] = 0;
            $phonecall['verification_error'] = $e->getMessage();
            $phonecall = (object)$phonecall;

            
        }

        return $phonecall;

    }

    function send_message($sender,$recipient,$message){

        $sid   = $this->sid;
        $token = $this->token;
        $from  = $this->from;

                
        $client = new Services_Twilio($sid, $token);

        $message = $client->account->messages->sendMessage($from, $recipient, "From ". $sender.",\n".$message."\n\n"."Lookna.com");
     
        if($message->status === 'queued'){
            return true;
        } 
    }

}
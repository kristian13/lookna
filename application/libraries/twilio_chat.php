<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	
	    
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\IpMessagingGrant;
use Twilio\Rest\Client;

class Twilio_chat {
 
  private $ci;

  private $identity;
  private $endpointId;
  private $token;

  private $sid;
  private $service_sid;  
  private $api_key;
  private $api_secret;
	
	function __construct() {   

      require_once 'third-party/vendor/autoload.php';
		
      $this->ci =& get_instance();
      $this->ci->load->config('twilio');

      $this->sid         = $this->ci->config->item('sid');
      $this->service_sid = $this->ci->config->item('service_sid');
      $this->api_key     = $this->ci->config->item('api_key');
      $this->api_secret  = $this->ci->config->item('api_secret');

	}

  public function chat_name(){  
    return $this->ci->session->userdata('username');
  }

  public function get_device(){

      $this->ci->load->library('user_agent');

      if($this->ci->agent->is_browser())
      {
        return $this->ci->agent->browser().' '.$this->ci->agent->version();
      }
      elseif ($this->ci->agent->is_robot())
      {
        return $this->ci->agent->robot();
      }
      elseif ($this->ci->agent->is_mobile())
      {
        return $this->ci->agent->mobile();
      }
      else
      {
        return 'Unidentified User Agent';
      }
  }

  public function serialized($advetiser_id=null){

      // An identifier for your app - can be anything you'd like
      $appName = 'TwilioChatDemo';

      // Get user_id
      $this->identity = $this->chat_name();
      
      // Get User Device
      $deviceId = $this->get_device();
      
      // The endpoint ID is a combination of the above
      $this->endpointId = $appName . ':' . $this->identity . ':' . $deviceId;

      // config params
      $TWILIO_ACCOUNT_SID     = $this->sid;
      $TWILIO_IPM_SERVICE_SID = $this->service_sid;
      $TWILIO_API_KEY         = $this->api_key;
      $TWILIO_API_SECRET      = $this->api_secret;
      // Create access token, which we will serialize and send to the client
      $token = new AccessToken(
          $TWILIO_ACCOUNT_SID, 
          $TWILIO_API_KEY, 
          $TWILIO_API_SECRET, 
          3600, 
          $this->identity
      );

      $this->token = $token;

      // Grant access to IP Messaging
      $grant = new IpMessagingGrant();
      $grant->setServiceSid($this->service_sid);
      $grant->setEndpointId($this->endpointId);

      $token->addGrant($grant);    

      // return serialized token and the user's randomly generated ID  
      echo json_encode(array(
          'identity'      => $this->identity,
          'advertiser_id' => (int)$advetiser_id,
          'token'         => $token->toJWT(),
      ));
  }

  public function send_notify($channel,$user){

      $this->load->library('twilio');
      
      $CHANNEL = $channel;
      $USER    = $user;

      // Find your Account Sid at https://www.twilio.com/console/account/settings
      $TWILIO_ACCOUNT_SID = $this->sid;

      // Create an API Key and Secret at https://www.twilio.com/console/chat/dev-tools/api-keys
      $TWILIO_API_KEY    = $this->api_key;
      $TWILIO_API_SECRET = $this->api_secret;
      
      // Your Chat Service SID from https://www.twilio.com/console/chat/services
      $CHAT_SERVICE_SID = $this->service_sid;

      $token = new AccessToken(
          $this->sid, 
          $this->api_key, 
          $this->api_secret, 
          3600, 
          $this->identity
      );
      
  }


}

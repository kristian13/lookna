<?php
class Send_messages_model extends CI_Model {

   public function insert_message($message,$channel_id){        
      $data = array(
          'user_id'            => $this->session->userdata('id'),
      		'message'            => $message,
        	'time_created'       => date("Y-m-d H:i:s"),
      		'time_updated'       => date("Y-m-d H:i:s"),
      		'channel_message_id' => $channel_id
      );
  		$result = $this->db->insert('send_messages',$data);
      return $result;
   }

}
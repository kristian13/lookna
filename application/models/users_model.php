<?php
class Users_model extends CI_Model {
	

	public function getUserInfo($username,$password){
           
         $this->db->from('users');
         $this->db->where('username', $username);
         $this->db->where('password', md5($password));
         $result = $this->db->get()->result_array();
         if($result[0]){
         	 $this->session->set_userdata($result[0]);
         	 return true;
         }
    return false;
	}

    public function get_unread_message(){

          $this->db->select('*');
          $this->db->from('users');
          $this->db->join('messages_status ms' , 'ms.reciever_id = users.id');
          $this->db->join('channel_messages cm', 'cm.id = ms.channel_message_id');
          $this->db->where('users.id',$this->session->userdata('id'));
          $this->db->order_by('ms.status','asc');
          $results = $this->db->get()->result_array();
        
          for($a=0; $a<count($results); $a++){
                $this->db->select('*');
                $this->db->from('users');
                $this->db->where('id',$results[$a]['sender_id']);
                $res = $this->db->get()->result_array();
                $results[$a]['sender_name'] = $res[0]['username'];
                $results[$a]['sender_number'] = $res[0]['number'];
          } 
  
      return $results;
    }

    public function update_number($number,$id){

        $update = array(
            'number' => $number
        );
        $this->db->where('id',$id);
        $result = $this->db->update('users',$update);
      return $result;
    }

    public function get_number($id){
        $this->db->select('number');
        $this->db->where('id',$id);
        $this->db->from('users');
        $result = $this->db->get()->result_array();
       
       return $result[0]['number'];
    }

}
<?php
class Channel_messages_model extends CI_Model {

	
	public function __construct(){
	
	}
  
  public function create_channel($channel_name,$channel_sid){
    

     $channel = $this->find_channel($channel_name,$channel_sid);

     if(!$channel){
        
        $insert = array(
           'channel'     => $channel_name,
           'channel_sid' => $channel_sid,
        );

        $this->db->insert('channel_messages',$insert);
        $result = $this->db->insert_id();
        
        return $result;
     
     } else {

      return $channel[0]['id'];
     }
     
  }

  public function find_channel($channel_name,$channel_sid){
       $this->db->select("*");
       $this->db->where('channel',$channel_name);
       $this->db->where('channel_sid',$channel_sid);
       $this->db->from('channel_messages');
       $result = $this->db->get()->result_array();
    return $result;
  }

 

}
<?php
class Messages_status_model extends CI_Model {


  public function check_sender_reciever($sender_id,$reciever_id){
     $this->db->from('messages_status');
     $this->db->where('sender_id'  ,$sender_id);
     $this->db->where('reciever_id',$reciever_id);
     $result = $this->db->get()->result_array();
     return $result;
  }

  public function message_status($sender_id,$reciever_id,$channel_id){

      $message_status = $this->check_sender_reciever($sender_id,$reciever_id);

      if(!$message_status){
          //insert if not existed
          $insert = array(
                'sender_id'          => $sender_id,
                'reciever_id'        => $reciever_id,
                'channel_message_id' => $channel_id,
                            'status' => true
          );
  
          $this->db->insert('messages_status',$insert);

      } else {

          //update if existed
          $this->db->set('status', true);
          $this->db->where('sender_id',$sender_id);
          $this->db->where('reciever_id',$reciever_id);
          $this->db->where('channel_message_id',$channel_id);
          $this->db->update('messages_status');
      }

  }

  public function get_all_new_message($reciever_id){
         $this->db->select('*');
         $this->db->from('messages_status ms');
         $this->db->join('channel_messages cm', 'cm.id = ms.channel_message_id');
         $this->db->where('status',1);
         $this->db->where('reciever_id',$reciever_id);
         $result = $this->db->get()->result_array();
      return $result;
  }

  public function set_message_seen($reciever_id,$sender_id,$channel_id){
            
      $this->db->set('status',0);
      $this->db->where('sender_id',$sender_id);
      $this->db->where('reciever_id',$reciever_id);
      $this->db->where('channel_message_id',$channel_id);
      $result = $this->db->update('messages_status');
            
    return $result;
  }

}
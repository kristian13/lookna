<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

  public function __construct(){
  	     parent::__construct();   
         $this->load->helper(array('form', 'url'));
  }

  public function return_identity(){ 
    $this->load->library('twilio_chat');
    
    if(isset($_GET['advertiser_id'])){
       $this->twilio_chat->serialized($_GET['advertiser_id']);
    }else{
      $this->twilio_chat->serialized();
    } 
  }

 
  public function send_message() {
      $this->load->library('twilio');

      $from      = $this->input->post('from');
      $recipient = $this->input->post('recipient');
      $message   = $this->input->post('message');

      if($this->twilio->send_message($from,$recipient,$message)){
        echo json_encode(array('status'=>'send')); 
      }else{
        echo json_encode(array('status'=>'error'));
      }
          
  }

  public function chat_upload_picture(){

      $config['upload_path']   = './uploads/pictures'; 
      $config['allowed_types'] = 'gif|jpg|png'; 
      $config['max_size']      = 512000; 
      $config['max_width']     = 0; 
      $config['max_height']    = 0;  

      $this->load->library('upload', $config);

      //print_r($_FILES['file']);
      
      if(!$this->upload->do_upload('file')) {
         $data = array('error' => $this->upload->display_errors());   
         
         $data['error']  = strip_tags($data['error']); 
         $data['status'] = 'bad';
      
         echo json_encode($data);
      } else { 
         $file_data = $this->upload->data(); 
         
         $data['img_path'] = base_url().'uploads/pictures/'.$file_data['file_name'];
         $data['status'] = 'ok';
         
         echo json_encode($data,JSON_UNESCAPED_SLASHES);
      }  
      
  }

  public function chat_upload_video(){

      $config['upload_path']   = './uploads/videos'; 
      $config['allowed_types'] = 'mp4';  
      $config['max_size']      = 625000; 
      $config['max_width']     = 0; 
      $config['max_height']    = 0;  

      $this->load->library('upload', $config);
     
      
      if(!$this->upload->do_upload('file')) {
         $data = array('error' => $this->upload->display_errors());   
         
         $data['error']  = strip_tags($data['error']); 
         $data['status'] = 'bad';
      
         echo json_encode($data);
      } else { 
         
         $file_data = $this->upload->data(); 
         
         $data['video_path'] = base_url().'uploads/videos/'.$file_data['file_name'];
         $data['status'] = 'ok';
         
         echo json_encode($data,JSON_UNESCAPED_SLASHES);
      }  
      
  }

  public function chat_upload_audio() {

      $config['upload_path']   = './uploads/audios'; 
      $config['allowed_types'] = 'mp3'; 
      $config['max_size']      = '1000000'; 
      $config['max_width']     = 0; 
      $config['max_height']    = 0;  

      $this->load->library('upload', $config);
     
      
      if(!$this->upload->do_upload('file')) {
         $data = array('error' => $this->upload->display_errors());   
         
         $data['error']  = strip_tags($data['error']); 
         $data['status'] = 'bad';
      
         echo json_encode($data);

      } else { 
         
         $file_data = $this->upload->data(); 
         
         $data['audio_path'] = base_url().'uploads/audios/'.$file_data['file_name'];
         $data['status'] = 'ok';
         
         echo json_encode($data,JSON_UNESCAPED_SLASHES);
      } 
  }


  public function create_channel(){

      $this->load->model('channel_messages_model');

      $channel_name = $this->input->post('channel_name');
      $channel_sid  = $this->input->post('channel_sid');

      $result = $this->channel_messages_model->create_channel($channel_name,$channel_sid);
    echo json_encode($result);
  }

  public function message_status(){

    $this->load->model('messages_status_model');

    $sender_id   = $this->input->post('sender_id');
    $reciever_id = $this->input->post('reciever_id');       
    $channel_id  = $this->input->post('channel_id');

    $this->messages_status_model->message_status($sender_id,$reciever_id,$channel_id);

  }
  

  public function get_all_new_message(){
    $this->load->model('messages_status_model');

    $reciever_id = $this->input->post('reciever_id');
    $result = $this->messages_status_model->get_all_new_message($reciever_id);

    echo json_encode($result);
  }

  public function set_message_seen(){

     $this->load->model('messages_status_model');

     $reciever_id  = $this->input->post('reciever_id');
     $sender_id    = $this->input->post('sender_id');
     $channel_id   = $this->input->post('channel_id');

     $result = $this->messages_status_model->set_message_seen($reciever_id,$sender_id,$channel_id);

    return $result;
  }


  public function send_notify(){

      $this->load->library('twilio_chat');

      $channel = $this->input->post('channel');
      $user    = $this->input->post('user');

      print_r($this->twilio_chat->send_notify($channel,$user));

     

  }
  
  

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

   
    private $username;
    private $password;
    private $user_id;
    private $role_id;

	public function __construct() {
		parent::__construct();
		

		$this->auth();

		$this->load->model('users_model');
		$this->user_id  = $this->session->userdata('id');
		$this->role_id  = $this->session->userdata('level');
		$this->username = $this->input->post('username');
		$this->password = $this->input->post('password');

		$this->load->model('channel_messages_model');
   	    $this->load->model('users_model');
   	    $this->load->model('messages_status_model');
	}

	public function home() { 

		$this->load->library('Twilio_chat');
		
		$result = $this->users_model->get_unread_message();

		$listing_number =  $this->users_model->get_number(2);
		
		$data['messages']       = $result; 
		$data['listing_number'] = $listing_number;

  
        $this->load->view('home',$data);
	}

	public function index(){

		$this->load->library('form_validation');
	    if($this->input->post('login'))
	    {
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == TRUE)
            {
                $result = $this->users_model->getUserInfo($this->username,$this->password);    
                if($result)
                {
                	redirect('/site/home');
                }
                    redirect('/site/index');
            }
	    }
		$this->load->view('index');
	}

	public function edit(){

		$this->load->library('Twilio_chat');

		
		if($this->input->post()){
			$this->users_model->update_number($this->input->post('number'),$this->user_id);
			echo "<script type='text/javascript'>alert('Number Updated..');</script>";
		}


		$result = $this->users_model->get_unread_message();
		$number = $this->users_model->get_number($this->user_id);
		$data['messages'] = $result; 
		$data['number']   = $number;

		$this->load->view('edit',$data);
	}

    public function logout(){
    	$this->session->unset_userdata('id');
    	$this->session->unset_userdata('username');
    	$this->session->unset_userdata('password');
    	$this->session->unset_userdata('level');

      	redirect('/site/index');
    }

	public function auth(){
		if(!$this->session->userdata('id')) {
				$pages = array('home','edit');
				if( in_array($this->uri->segment(2),$pages) ){
					redirect('/site/index','refresh');
				}
		}
	}
}

<!DOCTYPE html>
<html>
<head>
  <title>Twilio IP Messaging Quickstart</title>
  <link rel="shortcut icon" href="//www.twilio.com/marketing/bundles/marketing/img/favicons/favicon.ico">
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url(); ?>css/chat.css">
  
  <script src="<?= base_url(); ?>js/twilio-chat.min.js"></script>
  <script src="<?= base_url(); ?>js/twilio-common.min.js"></script>
  <script src="<?= base_url(); ?>js/twilio-ip-messaging.min.js"></script>
  <script src="<?= base_url(); ?>js/jquery.min.js"></script>
</head>

<body>

  <header>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="home">Brandname</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="edit">Edit Profile</a></li>
          <li class="dropdown" id="history_chats" user_id="<?= $this->session->userdata('id'); ?>">
            <?php
              $count = 0;
              foreach($messages as $message){
                  if ($message['status'] == 1){
                      $count++;
                  };
              }
            ?>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Messages <span class="newMessageInfo badge badge-error"></span>
            </a>
            <ul class="dropdown-menu scrollable-menu">
                <?php foreach($messages as $message): ?>
                <li class="<?=$message['channel_sid']?>">
                     <a href="#" channel_name="<?=$message['channel']?>" channel_id="<?=$message['channel_message_id']?>" channel_sid="<?=$message['channel_sid']?>" sender_id="<?=$message['sender_id']?>" sender_number="<?=$message['sender_number']?>">
                         <div class="title pull-left">
                         <?= $message['sender_name']?>
                         </div>
                         <span class="badge badge-error pull-right"></span>
                         <br>
                     </a>
                     
                </li>
                <?php endforeach ?>
            </ul>
          </li>
          <?php if($this->session->userdata('id')){ ?>
              <li><a href="logout"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
           <?php } else {?>
              <li><a href="index"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
           <?php } ?>
        </ul>
      </div>
    </nav>
  </header>



  <h1><?= $this->session->userdata('username');?></h1>




 <?php if($this->session->userdata('username') != 'advertiser'){ ?>
   <div id="advertiser">
      <a href="#" listing_id="2" listing_number="<?= $listing_number; ?>" class="btn btn-primary">Send Message to Advertiser 1</a>
   </div>
 <?php } ?>
 



  <div id="bottom_append">
  </div>




  <!-- Chat Image Modal -->
  <div class="modal fade" id="chat_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" data-dismiss="modal">
      <div class="modal-content"  >              
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">
             <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
          </button>
          <img src="" class="imagePreview" style="width:100%; height:100%;">
        </div>           
      </div>
    </div>
  </div>

  <!-- Chat video Modal -->
  <div class="modal fade" id="chat_video" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" data-dismiss="modal">
      <div class="modal-content"  >              
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">
             <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
          </button>
          <video style="width:100%; height:100%;" preload="auto" controls>
            <source class="videoPreview" src="" type="video/mp4">
          </video>
        </div>           
      </div>
    </div>
  </div>

  <!-- Chat audio Modal -->
  <div class="modal fade" id="chat_audio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" data-dismiss="modal">
      <div class="modal-content"  > 
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">
             <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
          </button>
          <audio style="width:100%; height:100%;" preload="auto" controls>
            <source class="audioPreview" src="" type="audio/mp3">
          </audio>
        </div>           
      </div>
    </div>
  </div>

   <!-- Chat SMS Modal -->
  <div id="chat_sms" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">SMS Message</h4>
        </div>
        <div class="modal-body">
         <form>
           <input type="hidden" name="recipient" value="" id="recipient">
           <textarea rows="10" cols="30" placeholder="Enter your message here.."></textarea>
         </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary pull-left" id="chat_sms_send">Send</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
  
    </div>
  </div>


  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    

$(document).ready(function(){


    var queue = null;
    var page  = null;

    // GLOBAL VARIABLES
    
    var GlobalChannel = [];
    // Manages the state of our access token we got from the server
    var accessManager;
    // Our interface to the IP Messaging service
    var messagingClient;

    var userSessionId   = <?= $this->session->userdata('id'); ?>;
    var userSessionName = '<?= $this->session->userdata('username'); ?>';


    var $chatWindow;

    var clientServer;

    // The server will assign the Username and store that value here
    var username;

    var reciever_id;
    var sender_id;

    var advertiser_id;
    var advertiser_number;


    var channel_id;      
    var channel_info;
    var channel_name;
    var myChannelList = [];

    var $token;
    var total;

  
    function getToken(){
       $.getJSON('<?= base_url();?>api/return_identity',{},function(data){
          $token   = data.token;
          username = data.identity;
       });
    }
    getToken();

    // Helper function to print Business name
    function print(infoMessage, asHtml,num) {
        var $msg = $('<div class="info">');
        if (asHtml) {
            $msg.html(infoMessage);
        } else {
            $msg.text(infoMessage);
        }
        $chatWindow.closest('.col-md-2')
                       .find('#business_name')
                       .html('<div class="info">'+channel_info+
                               "<strong class='pull-right close_chat' number="+num+" style='padding-right:5px; cursor:pointer;'>x</strong>"+
                               "<strong class='pull-right min_chat' style='padding-right:5px; cursor:pointer;'>_</strong>"+
                             '</div>');

        $chatWindow.scrollTop($chatWindow[0].scrollHeight);
    }

    // Helper function to print chat message to the chat window
    function printMessage(fromUser, message,object) {

        extenName = message.split('.').pop();

        //responce on user side
        if (fromUser === username) {

            //if user send a picure
            if($.inArray(extenName,['jpeg','jpg','png','gif']) !== -1 ) {
                var $message   = $('<a href="#" class="chat_image"><img src="'+message+'"></a>');
                var $container = $('<div class="message-container me" message_sid="'+object.sid+'">');
                $container.append($message);
                $chatWindow.append($container);
            }
            
            //if user send video
            else if($.inArray(extenName,['mp4','mov']) !== -1 ) {
                
                if(extenName == 'mp4'){
                   var $message   = $('<a href="#" class="chat_video">'+
                                         '<video width="200" height="110" controls>'+
                                          '<source src="'+message+'" type="video/mp4">'+
                                         '</video>'+
                                      '</a>');
                   var $container = $('<div class="message-container me" message_sid="'+object.sid+'">').html($message);
                  
                   $chatWindow.append($container);

                   //$chatWindow.find('video').each(function(key,val){
                   //   val.load();
                   //});
                   
                }else {
                   var $message   = $('<a href="#" class="chat_video">'+
                                         '<video width="200" height="110" controls>'+
                                          '<source src="'+message+'" type="video/quicktime">'+
                                         '</video>'+
                                      '</a>');
                   var $container = $('<div class="message-container me" message_sid="'+object.sid+'">');
                   $container.append($message);
                   $chatWindow.append($container);
                
                }

            }
            
            //if user send audio
            else if($.inArray(extenName,['mp3','Ogg','Wav']) !== -1 ) {
                  
                  var $message   = $('<a href="#" class="chat_audio">'+
                                         '<audio controls style="width:200px; height:60px;">'+
                                           '<source src="'+message+'" type="audio/mp3">'+
                                         '</audio>'+
                                      '</a>');
                   var $container = $('<div class="message-container me" message_sid="'+object.sid+'">');
                   $container.append($message);
                   $chatWindow.append($container);
            }
            
            //if user send plain text
            else {
               var $message = $('<span class="message">').text(message);
               var $user = $('<span class="username">').text(fromUser);
               var $container = $('<div class="message-container me">');
               $container.append($message);
               $chatWindow.append($container);
            }

        //response to other client side
        }else{

            var $user = $('<span class="username">').text(fromUser +' : ');

            //if user send a picure
            if($.inArray(extenName,['jpeg','jpg','png','gif']) !== -1 ){
                var $message   = $('<a href="#" class="chat_image"><img src="'+message+'"></a>');
                var $container = $('<div class="message-container">');
                $container.append($user).append($message);
                $chatWindow.append($container);
            }
            
            //if user send video
            else if($.inArray(extenName,['mp4','mov']) !== -1 ) {
              
              if(extenName == 'mp4'){
                   var $message   = $('<a href="#" class="chat_video">'+
                                         '<video width="200" height="110" controls>'+
                                         '<source src="'+message+'" type="video/mp4">'+
                                         '</video>'+
                                      '</a>');
                   var $container = $('<div class="message-container">').html($message); 
                   $chatWindow.append($container);
                   
                   //$chatWindow.find('video').each(function(key,val){
                   //   val.load();
                   //});
                
                }else {
                  
                   var $message   = $('<a href="#" class="chat_video">'+
                                         '<video width="200" height="110" controls="controls">'+
                                         ' <source src="'+message+'" type="video/quicktime">'+
                                         '</video>'+
                                      '</a>');
                   var $container = $('<div class="message-container">');
                   $container.append($user).append($message);
                   $chatWindow.append($container);
                }

            }
            
            //if user send audio
            else if($.inArray(extenName,['mp3','Ogg','Wav']) !== -1 ) {
                  var $message   = $('<a href="#" class="chat_audio">'+
                                         '<audio controls style="width:200px; height:60px;">'+
                                           '<source src="'+message+'" type="audio/mp3">'+
                                         '</audio>'+
                                      '</a>');
                   var $container = $('<div class="message-container">');
                   $container.append($message);
                   $chatWindow.append($container);
            }
            
            //if user send plain text
            else {
              var $message = $('<span class="message">').text(message);
              var $user = $('<span class="username">').text(fromUser +' : ');
              var $container = $('<div class="message-container">');
              $container.append($user).append($message);
              $chatWindow.append($container);
            }

        }
       $chatWindow.scrollTop($chatWindow[0].scrollHeight);
    }


    function printMessageHistory(fromUser, message,object) {

        extenName = message.split('.').pop();

        //responce on user side
        if (fromUser === username) {

            //if user send a picure
            if($.inArray(extenName,['jpeg','jpg','png','gif']) !== -1 ) {
                var $message   = $('<a href="#" class="chat_image"><img src="'+message+'"></a>');
                var $container = $('<div class="message-container me" message_sid="'+object.sid+'">');
                $container.append($message);
                $chatWindow.prepend($container);
            }
            
            //if user send video
            else if($.inArray(extenName,['mp4','mov']) !== -1 ) {
                
                if(extenName == 'mp4'){
                   var $message   = $('<a href="#" class="chat_video">'+
                                         '<video width="200" height="110" controls>'+
                                          '<source src="'+message+'" type="video/mp4">'+
                                         '</video>'+
                                      '</a>');
                   var $container = $('<div class="message-container me" message_sid="'+object.sid+'">').html($message);
                  
                   $chatWindow.prepend($container);

                   //$chatWindow.find('video').each(function(key,val){
                   //   val.load();
                   //});
                   
                }else {
                   var $message   = $('<a href="#" class="chat_video">'+
                                         '<video width="200" height="110" controls>'+
                                          '<source src="'+message+'" type="video/quicktime">'+
                                         '</video>'+
                                      '</a>');
                   var $container = $('<div class="message-container me" message_sid="'+object.sid+'">');
                   $container.append($message);
                   $chatWindow.prepend($container);
                
                }

            }
            
            //if user send audio
            else if($.inArray(extenName,['mp3','Ogg','Wav']) !== -1 ) {
                  
                  var $message   = $('<a href="#" class="chat_audio">'+
                                         '<audio controls style="width:200px; height:60px;">'+
                                           '<source src="'+message+'" type="audio/mp3">'+
                                         '</audio>'+
                                      '</a>');
                   var $container = $('<div class="message-container me" message_sid="'+object.sid+'">');
                   $container.append($message);
                   $chatWindow.prepend($container);
            }
            
            //if user send plain text
            else {
               var $message = $('<span class="message">').text(message);
               var $user = $('<span class="username">').text(fromUser);
               var $container = $('<div class="message-container me" message_sid="'+object.sid+'">');
               $container.append($message);
               $chatWindow.prepend($container);
            }

        //response to other client side
        }else{

            var $user = $('<span class="username">').text(fromUser +' : ');

            //if user send a picure
            if($.inArray(extenName,['jpeg','jpg','png','gif']) !== -1 ){
                var $message   = $('<a href="#" class="chat_image"><img src="'+message+'"></a>');
                var $container = $('<div class="message-container">');
                $container.append($user).append($message);
                $chatWindow.prepend($container);
            }
            
            //if user send video
            else if($.inArray(extenName,['mp4','mov']) !== -1 ) {
              
              if(extenName == 'mp4'){
                   var $message   = $('<a href="#" class="chat_video">'+
                                         '<video width="200" height="110" controls>'+
                                         '<source src="'+message+'" type="video/mp4">'+
                                         '</video>'+
                                      '</a>');
                   var $container = $('<div class="message-container">').html($message); 
                   $chatWindow.prepend($container);
                   
                   //$chatWindow.find('video').each(function(key,val){
                   //   val.load();
                   //});
                
                }else {
                  
                   var $message   = $('<a href="#" class="chat_video">'+
                                         '<video width="200" height="110" controls="controls">'+
                                         ' <source src="'+message+'" type="video/quicktime">'+
                                         '</video>'+
                                      '</a>');
                   var $container = $('<div class="message-container">');
                   $container.append($user).append($message);
                   $chatWindow.prepend($container);
                }

            }
            
            //if user send audio
            else if($.inArray(extenName,['mp3','Ogg','Wav']) !== -1 ) {
                  var $message   = $('<a href="#" class="chat_audio">'+
                                         '<audio controls style="width:200px; height:60px;">'+
                                           '<source src="'+message+'" type="audio/mp3">'+
                                         '</audio>'+
                                      '</a>');
                   var $container = $('<div class="message-container">');
                   $container.append($message);
                   $chatWindow.prepend($container);
            }
            
            //if user send plain text
            else {
              var $message = $('<span class="message">').text(message);
              var $user = $('<span class="username">').text(fromUser +' : ');
              var $container = $('<div class="message-container">');
              $container.append($user).append($message);
              $chatWindow.prepend($container);
            }

        }
    }
    

    function updateTypingIndicator(member,boolean){
        if(boolean){
           $chatWindow.find('.'+member.identity).remove();
           var $container = '<small class='+member.identity+' style="background-color:white; padding-left:20px;">'+
                                '<i>'+member.identity+' is typing...'+'</i>'+
                            '</small>';
           $chatWindow.append($container);
           $chatWindow.scrollTop($chatWindow[0].scrollHeight);
        }else{
          $chatWindow.find('.'+member.identity).remove();
          $chatWindow.scrollTop($chatWindow[0].scrollHeight);
        }
    }


    function message_status(reciever_id,channel_id){

          //set message status
          $.ajax({
             url:'<?= base_url();?>api/message_status',
             type: 'POST',
             data:{
                      sender_id: userSessionId,
                    reciever_id: reciever_id,
                     channel_id: channel_id,
             },
          
             success:function(data){
                  console.log('message status update');
             }
          });

          //get new messages and set to my message notification
          // $.ajax({
          //   url:'<?= base_url();?>api/get_all_new_message',
          //   type: 'post',
          //   dataType:'json',
          //   data:{
          //            reciever_id: userSessionId,
          //   },
          //   success:function(data){
          //       var $count = null;
          //       $.each(data,function(key,val){
          //          $count++;
          //       })
          //       console.log(data);
          //      //if($count <= 0){
          //      //    $('#history_chats').find('span.badge').text('0');
          //      //}else{
          //      //   $('#history_chats').find('span.badge').html($count);  
          //      //}
          //   }
          // });
    }


    // Set up channel after it has been found
    function setupChannel(a) {
      //GlobalChannel.uniqueName;
      GlobalChannel[a].join().then(function(channel) {
        print('print the Name', true,a);
      });

      // Listen for new messages sent to the channel
      GlobalChannel[a].on('messageAdded', function(message) {
          message_status(reciever_id,channel_id);
          GlobalChannel[a].setAllMessagesConsumed();
          printMessage(message.author, message.body,message);
      });

      GlobalChannel[a].on('messageUpdated', function(message) {
          //updateMessage(message.author, message.body);
      });
      
      GlobalChannel[a].on('messageRemoved', function(message) {
          //removeMessage(message.author, message.body);
      });

      GlobalChannel[a].on('typingStarted', function(member) {
          //process the member to show typing
          updateTypingIndicator(member, true);
      });
      
      //set  the listener for the typing ended Channel event
      GlobalChannel[a].on('typingEnded', function(member) {
          //process the member to stop showing typing
          updateTypingIndicator(member, false);
      });

      // A channel's attributes or metadata have changed.
      GlobalChannel[a].on('updated', function(channel) {
        console.log('message status: consume');
      });

    }

    function processPage() {

        if (page.hasPrevPage) {
        
            //Move first loading and show
            $('#messages').find('#message_loading').prependTo('#messages > div').show();

            page.prevPage().then(function(messages){
                //overwrite previous chat history
                page = messages;

               
                for (i=(messages.items.length-1); i>=0; i--) {
                        var message  = messages.items[i]; 
                        printMessageHistory(message.author,message.body,message);
                } 
            });

            //Hide Loading..
            $('#messages').find('#message_loading').hide();

        }else{
              console.log('All message are shown..');
        }
    }

    function getChatHistory(a){

      GlobalChannel[a].getMessagesPaged(10)
                        .then(function(messages) {
                            
                            page = messages;                     

                            //console.log(messages);
                            for (i=(messages.items.length-1); i>=0; i--) {
                                    var message  = messages.items[i]; 
                                    printMessageHistory(message.author,message.body,message);
                            }
                            
                            $chatWindow.scrollTop($chatWindow[0].scrollHeight);

                        });

          //Set Message Status seen  
          //$.ajax({
          //   url:'<?= base_url(); ?>api/set_message_seen',
          //   type:'post',
          //   data:{ 
          //           reciever_id:userSessionId,
          //             sender_id:sender_id,
          //            channel_id:channel_id 
          //   },
          //   success:function(data){
          //  
          //       var $count = parseInt($('#history_chats').find('span.badge').text());
          //           
          //           $count--;
          //  
          //          if($count < 0 || $count == 0 ){
          //            $('#history_chats').find('span.badge').text('0');
          //          }else{
          //            $('#history_chats').find('span.badge').text($count);  
          //          }
          //   }
          //});


      //remove loading
      $('#messages').find('#message_loading').remove(); 
      
      //$('#history_chats > ul > li > a.'+val.sid+'').find('span.badge').text(null);
         
    }
    

    function return_channel_on_db(channel_sid,channel_name){
    
        var $sender_id = userSessionId;  

            $.ajax({
                   url:'<?= base_url();?>api/create_channel',
                   type: 'post',
                   dataType: 'json',
                   data:{
                            channel_name:channel_name,
                            channel_sid:channel_sid,
                   },
                   success:function(data){
                     channel_id = data;
               }
            });
    }
 
    function getUnconsumeMessage(){

              var options = ['silent'];

              clientServer    = new Twilio.Chat.Client($token,options);
              clientServer.initialize()
               .then(function(client) {
                 client.getUserChannels().then(function(data){
                     //count all new Message on each channels
                    
                     $.each(data.items,function(key,val){
                         val.messagesEntity.messagesListPromise.then(function(newMessage){
                              var newMessageId = newMessage.descriptor.last_event_id;
                              var oldMessageId = val.lastConsumedMessageIndex;
                              var newMessageCount = newMessageId - oldMessageId;
                               
                              total = newMessageCount + total;

                              if(newMessageCount > 0){
                                $('#history_chats > ul > li.'+val.sid+'').find('span.badge').text(newMessageCount).css("margin-top","2px");
                              }else{
                                $('#history_chats > ul > li.'+val.sid+'').find('span.badge').text(0);
                              }                                                                  
                        });                         
                     });

                });
              });

              clearInterval(window.ref);
              check_updates();
    }

   

    function Twilio_chat(i){

          //Set queue                     
          queue = true;


          //Initialize the IP messaging client
          accessManager   = new Twilio.AccessManager($token);
          var options = ['silent'];
          messagingClient = new Twilio.IPMessaging.Client(accessManager,options);
                                
          //getUnconsumeMessage();

          console.log(channel_name);

          var promise = messagingClient.getChannelByUniqueName(channel_name);

          promise.then(function(channel) {

              if (!channel) {

                  //If it doesn't exist, let's create it
                  messagingClient.createChannel({
                        uniqueName: channel_name
                  }).then(function(channel) {

                      //Create instance of channel on array key
                      GlobalChannel[i] = channel;

                      //Setup Channel
                      setupChannel(i);

                      //save on DB and pass the id on channel_id variable
                      return_channel_on_db(channel.sid,channel_name);
                      
                      //remove loading
                      $('#messages').find('#message_loading').remove();

                      //Set null on queue
                      queue = null;
                      i++;  
                  });

              } else {

                  //Connect again to the channel if found
                  GlobalChannel[i] = channel;

                  //Setup Channel
                  setupChannel(i);
                  
                  //after done loading history mark all message as consume                  
                  GlobalChannel[i].setAllMessagesConsumed();

                  //save on DB and pass the id on channel_id variable
                  return_channel_on_db(channel.sid,channel_name);

                  //Get previous Chats
                  getChatHistory(i);
                  
                  //set null on queue
                  queue = null;
                  i++;
              
              }

          });

                    
    }
    

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    function check_updates(){
       window.ref = window.setInterval(function() { getUnconsumeMessage(); }, 4000);
    }
    check_updates();
 



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Click on Send Message on Adversiter Entry Point

    <?php if($this->session->userdata('username') != 'advertiser') { ?>

       var c = 0;
       $('#advertiser > a').on('click',function(){

          $('#bottom_append').empty();

          if(queue == null){

                  //Set Variables
                  channel_info      = 'advertiser';
                  advertiser_id     = $(this).attr('listing_id');
                  reciever_id       = advertiser_id;
                  advertiser_number = $(this).attr('listing_number');
                  channel_name      = "CH_"+userSessionId+"_"+advertiser_id;

              var channel_sid       = "";

              $('#chat_sms').find('input#recipient').val(advertiser_number);

              var append =  "<div class='col-md-2'>"+
                              '<?php echo form_open_multipart("site/home", "id=upload_file"); ?>'+
                              "<div class='row' id='business_name'>"+
                              "</div>"+
                              "<div class='row' id='upload_button'>"+
                                  "<div class='fileUpload btn btn-default'>"+
                                     "<span>Picture </span>"+
                                     "<i class='fa fa-upload'></i><input type='file' name='picture' id='picture' class='upload'>"+
                                  "</div>"+
                                  "<div class='fileUpload btn btn-default'>"+
                                     "<span>Video </span>"+
                                     "<i class='fa fa-upload'></i><input type='file' name='video'   id='video' class='upload'></i>"+
                                  "</div>"+
                                  "<div class='fileUpload btn btn-default'>"+
                                     "<span>Audio </span>"+
                                     "<i class='fa fa-upload'></i><input type='file' name='audio'   id='audio' class='upload'></i>"+
                                  "</div>"+
                                  "<div class='fileUpload btn btn-default send_sms'>"+
                                     "<span >SMS </span>"+
                                     "<i class='fa fa-commenting-o aria-hidden='true'></i></i>"+
                                  "</div>"+
                              "</div>"+
                              "<div class='row' id='messages' array_id='"+c+"' channel_name='"+channel_name+"' channel_sid='"+channel_sid+"'>"+
                                "<div id='messages_"+c+"'>"+
                                  "<div id='message_loading' style='text-align:center; margin-top:10px;'>"+
                                    "<img src='<?= base_url()?>/img/preloader.gif' width='170' height='150'>"+
                                  "</div>"+
                                "</div>"+
                                 "<input type='text' name='chat' id='chat-input' class='chat_input' placeholder='say something'>"+
                              "</div>"+
                              "</form>"+
                            "</div>";

              $('#bottom_append').append(append);
              
              // Get handle to the chat div 
              $chatWindow = $('#messages_'+c);
            
              
              Twilio_chat(c);

              //Get Previous Chat when Scroll Top
              $("#messages > [id^=messages_]").on('scroll', function(){    
                  if( $(this).scrollTop() == 0){
                      processPage(page);
                  }
              });


          }else{
             //do nothing
          }

       });

    <?php } ?>
 
      



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    //Click on main Menu

    // Incremantal Value 
    var a = 0;

    //On Click Previous Message On Menu
    $('.scrollable-menu > li').on('click',function(){

        $('#bottom_append').empty();

        if(queue == null){

                //Overwrite Global Variables
                channel_name = $(this).find('a').attr('channel_name');
                channel_info = $(this).find('a').text();
                reciever_id  = $(this).find('a').attr('sender_id');
                sender_id    = $(this).find('a').attr('sender_id');
                channel_id   = $(this).find('a').attr('channel_id');
            var channel_sid  = $(this).find('a').attr('channel_sid');

           $('#recipient').val($(this).find('a').attr('sender_number'));

            var append =  "<div class='col-md-2'>"+
                            '<?php echo form_open_multipart("site/home", "id=upload_file"); ?>'+
                            "<div class='row' id='business_name'>"+
                            "</div>"+
                            "<div id='upload_button'>"+
                                "<div class='fileUpload btn btn-default'>"+
                                   "<span>Picture </span>"+
                                   "<i class='fa fa-upload'></i><input type='file' name='picture' id='picture' class='upload'>"+
                                "</div>"+
                                "<div class='fileUpload btn btn-default'>"+
                                   "<span>Video </span>"+
                                   "<i class='fa fa-upload'></i><input type='file' name='video'   id='video' class='upload'></i>"+
                                "</div>"+
                                "<div class='fileUpload btn btn-default'>"+
                                   "<span>Audio </span>"+
                                   "<i class='fa fa-upload'></i><input type='file' name='audio'   id='audio' class='upload'></i>"+
                                "</div>"+
                                "<div class='fileUpload btn btn-default send_sms'>"+
                                   "<span >SMS </span>"+
                                   "<i class='fa fa-commenting-o aria-hidden='true'></i></i>"+
                                "</div>"+
                            "</div>"+
                            "<div class='row' id='messages' array_id='"+a+"' channel_name='"+channel_name+"' channel_sid='"+channel_sid+"'>"+
                              "<div id='messages_"+a+"'>"+
                                "<div id='message_loading' style='text-align:center; margin-top:10px;'>"+
                                  "<img src='<?= base_url()?>/img/preloader.gif' width='170' height='150'>"+
                                "</div>"+
                              "</div>"+
                              "<input type='text' name='chat' id='chat-input' class='chat_input' placeholder='say something'>"+
                            "</div>"+
                            "</form>"+
                          "</div>";

            $('#bottom_append').append(append);
            
            // Get handle to the chat div 
             $chatWindow = $('#messages_'+a);
          
            
            Twilio_chat(a);

            //Get Previous Chat when Scroll Top
            $("#messages > [id^=messages_]").on('scroll', function(){    
                if( $(this).scrollTop() == 0){
                    processPage(page);
                }
            });
            

        }else{
           //do nothing
        }

    });//end scrollable-menu









/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //Bubble Events

      //send picture
      $('body').on('change','#picture',function(e){

                  var id = $(this).closest('form').find('#messages').attr('array_id');

                  var file_data = $(this).prop("files")[0];
                  var form_data = new FormData();
                  form_data.append('file', file_data);

                  if(queue == null){
                      $.ajax({
                         url: "<?php echo base_url();?>api/chat_upload_picture",
                         data: form_data,
                         contentType: false,
                         processData: false, 
                         type:'POST',
                         dataType:'json',
                         success: function(data){
                            if(data.status == 'ok'){
                                 GlobalChannel[id].sendMessage(data.img_path.toString());
                                 $('#picture').val(null);
                            }else {
                                alert(data.error);
                            }
                         }
                      })
                  }
                  
      });

      //send video
      $('body').on('change','#video',function(e){

                  var id = $(this).closest('form').find('#messages').attr('array_id');

                  var file_data = $(this).prop("files")[0];
                  var form_data = new FormData();
                  form_data.append('file', file_data);
                  
                  if(queue == null){
                      $.ajax({
                         url: "<?php echo base_url();?>api/chat_upload_video",
                         data: form_data,
                         contentType: false,
                         processData: false, 
                         type:'POST',
                         dataType:'json',
                         success: function(data){
                            if(data.status == 'ok'){
                                GlobalChannel[id].sendMessage(data.video_path.toString());
                                $('#video').val(null);
                                $('#bottom_append').find('video').last().load();
                            }else {
                                alert(data.error);
                            }
                         }
                      })
                  }
                  
      });

      //send audio
      $('body').on('change','#audio',function(e){

                  var id = $(this).closest('form').find('#messages').attr('array_id');

                  var file_data = $(this).prop("files")[0];
                  var form_data = new FormData();
                  form_data.append('file', file_data);
                  
                  if(queue == null){
                      $.ajax({
                         url: "<?php echo base_url();?>api/chat_upload_audio",
                         data: form_data,
                         contentType: false,
                         processData: false, 
                         type:'POST',
                         dataType:'json',
                         success: function(data){
                            if(data.status == 'ok'){
                                GlobalChannel[id].sendMessage(data.audio_path);
                                $('#audio').val(null);
                                $('#bottom_append').find('audio').last().load();
                            }else {
                                alert(data.error);
                            }
                         }
                      })
                  }
                  
      });
             
      // Send a new message on channel
      $('body').on('keydown','#chat-input', function(e) {
                 var id = $(this).parent().attr('array_id');
                  if (e.keyCode == 13 && queue == null) {  
                      e.preventDefault(); 

                     if($(this).val().length > 0){
                       
                        GlobalChannel[id].sendMessage($(this).val());
                       
                       $(this).val('');
                     }

                  }else{
                    
                  if (queue == null)
                      GlobalChannel[id].typing();
                  }           

                  if(e.keyCode == 13 && queue == true){
                    alert('Wait..');
                    e.preventDefault();
                  }
      });
              
      //Close chat
      $('body').on('click','.close_chat',function(){
          $('#bottom_append > div').remove();
      });

      $('body').on('click','.min_chat',function() {
        var state = $(this).data('state');

            switch(state){
              case 1 :
                      case 
                           undefined : $(this).data('state', 2);
                           $(this).closest('.col-md-2').find('#messages > div,input').hide();

              break;

              case 2 : 
                           $(this).data('state', 1); 
                           $(this).closest('.col-md-2').find('#messages > div,input').show();
              break;
            }

      });

      //Show picture when on Chat window picure is click
      $('body').on('click','.chat_image',function() {
        $('.imagePreview').attr('src', $(this).find('img').attr('src'));
        $('#chat_image').modal('show'); 
      });

      //Show video when on Chat window video is click
      $('body').on('click','.chat_video',function() {
        $(this).find('video').load();
        $('.videoPreview').attr('src', $(this).find('source').attr('src'));
        $('#chat_video').modal('show');
        $('#chat_video').find('video')[0].load();
      });

      //Show audio when on Chat window audio is click
      $('body').on('click','.chat_audio',function() {
        $('.audioPreview').attr('src', $(this).find('source').attr('src'));
        $('#chat_audio').modal('show');
        $('#chat_audio').find('audio').load();
      });

      //Show Chat SMS window when click
      $('body').on('click','.send_sms',function(){
        
        $('#chat_sms').modal('show');
      });

      $('body').on('click','#chat_sms_send',function(){

            var number  = $(this).closest('.modal-content').find('input');
            var message = $(this).closest('.modal-content').find('textarea');

            $('#chat_sms').find('.modal-body')
                            .append('<div class="alert alert-info">Sending message please wait...</div>');

            $.ajax({
                url:'<?= base_url();?>api/send_message',
                type:'post',
                dataType: 'json',
                data:{
                           from: '<?= $this->session->userdata('username')?>',
                      recipient: number.val(),
                        message: message.val()
                },
                success:function(data){
                  //console.log(data);
                  if(data.status == 'send'){

                      $('#chat_sms').find('.modal-body > div')
                                       .removeClass('alert-info')
                                       .addClass('alert-success')
                                          .html('Message Send')
                                            .delay(5000)
                                            .fadeOut(200,function(){
                                               $(this).remove();
                                            }); 
                      message.val(null);
                      
                      
                  }else {

                       $('#chat_sms').find('.modal-body > div')
                                       .removeClass('alert-info')
                                       .addClass('alert-danger')
                                          .html('Message not send \n Please try again later.')
                                            .delay(5000)
                                            .fadeOut(200,function(){
                                               $(this).remove();
                                            }); 
                      
                  }

                }
            });

      });

});
     

  </script>
</body>
</html>